package ru.t1.kharitonova.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldException{

    public ProjectIdEmptyException() {
        super("Error! Project id is empty.");
    }

}
