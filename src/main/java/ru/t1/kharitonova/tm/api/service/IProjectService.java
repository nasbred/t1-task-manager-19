package ru.t1.kharitonova.tm.api.service;

import ru.t1.kharitonova.tm.enumerated.Sort;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project>{

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);
}
