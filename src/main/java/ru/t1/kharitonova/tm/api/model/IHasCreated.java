package ru.t1.kharitonova.tm.api.model;

import java.util.Date;

public interface IHasCreated {
    Date getCreated();

    void setCreated(Date created);
}
