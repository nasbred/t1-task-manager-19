package ru.t1.kharitonova.tm.api.repository;

import ru.t1.kharitonova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    List<Task> findAllByProjectId(String projectId);

}
