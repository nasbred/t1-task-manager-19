package ru.t1.kharitonova.tm.api.service;

public interface IProjectTaskService {
    void bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    void unbindTaskToProject(String projectId, String taskId);
}
