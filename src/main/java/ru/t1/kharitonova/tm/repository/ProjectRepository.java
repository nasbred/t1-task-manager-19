package ru.t1.kharitonova.tm.repository;

import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.model.Project;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public boolean existsById(final String projectId) {
        for (final Project project: records) {
            if (project.getId().equals(projectId)) return true;
        }
        return false;
    }

}
