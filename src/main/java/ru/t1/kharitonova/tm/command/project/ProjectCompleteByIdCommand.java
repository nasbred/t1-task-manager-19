package ru.t1.kharitonova.tm.command.project;

import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand{

    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.COMPLETED);
    }

}
