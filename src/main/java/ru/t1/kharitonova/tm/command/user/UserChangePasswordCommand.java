package ru.t1.kharitonova.tm.command.user;

import ru.t1.kharitonova.tm.exception.user.AccessDeniedException;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.util.HashUtil;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand{

    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

    @Override
    public String getName() {
        return "user-change-password";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        final String userId = serviceLocator.getAuthService().getUserId();

        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        final String oldPassword = TerminalUtil.nextLine();
        final String hash = HashUtil.salt(oldPassword);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();

        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, newPassword);
    }

}
