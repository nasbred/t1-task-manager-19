package ru.t1.kharitonova.tm.command.project;

import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand{

    @Override
    public String getDescription() {
        return "Create project.";
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        getProjectService().create(name, description);
    }

}
