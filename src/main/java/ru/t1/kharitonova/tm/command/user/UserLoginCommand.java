package ru.t1.kharitonova.tm.command.user;

import ru.t1.kharitonova.tm.api.service.IAuthService;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand{

    @Override
    public String getDescription() {
        return "Login user.";
    }

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
