package ru.t1.kharitonova.tm.command.project;

import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand{

    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().removeByIndex(index);
        getProjectTaskService().removeProjectById(project.getId());
    }

}
