package ru.t1.kharitonova.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand{

    @Override
    public String getDescription() {
        return "Clear tasks.";
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        getTaskService().clear();
    }

}
