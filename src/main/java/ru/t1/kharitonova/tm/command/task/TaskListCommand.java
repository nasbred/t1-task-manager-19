package ru.t1.kharitonova.tm.command.task;

import ru.t1.kharitonova.tm.enumerated.Sort;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand{

    @Override
    public String getDescription() {
        return "Display list tasks.";
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(sort);
        int index = 1;
        for (final Task task: tasks){
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

}
